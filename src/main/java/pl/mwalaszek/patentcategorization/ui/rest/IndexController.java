package pl.mwalaszek.patentcategorization.ui.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.mwalaszek.patentcategorization.domain.Patent;
import pl.mwalaszek.patentcategorization.prediction.LogisticRegressionService;
import pl.mwalaszek.patentcategorization.prediction.RandomForrestService;
import pl.mwalaszek.patentcategorization.prediction.SVCService;

@Controller
public class IndexController {
    @Autowired
    private LogisticRegressionService logisticRegressionService;

    @Autowired
    private SVCService svcService;

    @Autowired
    private RandomForrestService randomForrestService;

    @GetMapping("/index")
    public String getIndex(Model model) {
        model.addAttribute("logisticRegressionService", logisticRegressionService);
        model.addAttribute("svcService", svcService);
        model.addAttribute("randomForrestService", randomForrestService);
        model.addAttribute("randomForrestMap", randomForrestService.getLabelPrecisionMap());
        model.addAttribute("logisticMap", logisticRegressionService.getLabelPrecisionMap());
        model.addAttribute("svcMap", svcService.getLabelPrecisionMap());
        model.addAttribute("patent", new Patent());
        return "index";
    }

    @RequestMapping(value = "/predict", method=RequestMethod.POST)
    public String predict(RedirectAttributes redirectAttributes, @ModelAttribute(value = "patent") Patent patent) {
        redirectAttributes.addFlashAttribute("logisticRegressionPrediction", logisticRegressionService.predictCategory(patent));
        redirectAttributes.addFlashAttribute("randomForrestPrediction", randomForrestService.predictCategory(patent));
        redirectAttributes.addFlashAttribute("svcPrediction", svcService.predictCategory(patent));

        return "redirect:/index";
    }
}