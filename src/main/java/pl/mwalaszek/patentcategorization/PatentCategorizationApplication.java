package pl.mwalaszek.patentcategorization;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationEventPublisher;
import pl.mwalaszek.patentcategorization.data.DataService;
import pl.mwalaszek.patentcategorization.data.repository.XodusPatentRepository;

import java.io.IOException;

@SpringBootApplication
public class PatentCategorizationApplication implements CommandLineRunner {
    private static final Logger LOG = Logger.getLogger(PatentCategorizationApplication.class);
    @Autowired
    private XodusPatentRepository patentRepository;
    @Autowired
    private DataService dataService;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public static void main(String[] args) {
		SpringApplication.run(PatentCategorizationApplication.class, args);
	}

	@Override
	public void run(String... args) {
	    if (patentRepository.findAll().isEmpty()){
            try {
                dataService.intiDatabase();
            } catch (IOException e) {
                LOG.error("Database initialization failed. Application will not work.");
            }
        }
        applicationEventPublisher.publishEvent(new DataInitializedEvent(this));
    }
}