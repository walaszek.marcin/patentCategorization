package pl.mwalaszek.patentcategorization;

import org.springframework.context.ApplicationEvent;

public class DataInitializedEvent extends ApplicationEvent {
    public DataInitializedEvent(Object source) {
        super(source);
    }
}
