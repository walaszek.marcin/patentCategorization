package pl.mwalaszek.patentcategorization.prediction.helpers;

import com.google.common.base.Stopwatch;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.feature.Word2Vec;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.sql.*;
import org.springframework.stereotype.Service;
import pl.mwalaszek.patentcategorization.domain.Patent;

import java.io.Serializable;
import java.util.Collections;

@Service
public class VectorService implements Serializable {
    private SparkSession sparkSession;
    private Word2VecModel abstractModel;
    private Word2VecModel inventorsModel;

    public VectorService(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public Dataset<Row> transformToVector(Dataset<Patent> patentDataset) {
        Stopwatch timer = Stopwatch.createStarted();
        PatentTokenizer patentTokenizer = new PatentTokenizer();
        Dataset<Row> rowDataset = patentTokenizer.tokenizeAndRemoveStopWords(patentDataset);
        rowDataset = patentTokenizer.tokenizeInventors(rowDataset);

        Word2Vec word2Vec = new Word2Vec();
        word2Vec.setVectorSize(200);
        word2Vec.setMinCount(1);
        word2Vec.setNumPartitions(2);
        word2Vec.setInputCol("without_stop_words");
        word2Vec.setOutputCol("vector_abstract");

        abstractModel = word2Vec.fit(rowDataset);
        rowDataset = abstractModel.transform(rowDataset).drop("pubAbstract", "words", "without_stop_words");
        rowDataset = transformInventorsToVector(rowDataset);

        VectorAssembler assembler = new VectorAssembler()
                .setInputCols(new String[]{"vector_abstract", "vector_inventors"})
                .setOutputCol("vector");
        rowDataset = assembler.transform(rowDataset).drop("vector_abstract", "vector_inventors");
        System.out.println("Vector conversion time: " + timer.stop());
        return rowDataset;
    }

    public Dataset<Row> transformToVector(Patent patent){
        PatentTokenizer patentTokenizer = new PatentTokenizer();
        Encoder<Patent> patentEncoder = Encoders.bean(Patent.class);
        Dataset<Patent> patentDataset = sparkSession.createDataset(
                Collections.singletonList(patent),
                patentEncoder
        );

        Dataset<Row> rowDataset = patentTokenizer.tokenizeAndRemoveStopWords(patentDataset);
        rowDataset = patentTokenizer.tokenizeInventors(rowDataset);

        rowDataset = abstractModel.transform(rowDataset).drop("pubAbstract", "words", "without_stop_words");
        rowDataset = inventorsModel.transform(rowDataset).drop("inventors_tokenized");

        VectorAssembler assembler = new VectorAssembler()
                .setInputCols(new String[]{"vector_abstract", "vector_inventors"})
                .setOutputCol("vector");
        rowDataset = assembler.transform(rowDataset).drop("vector_abstract", "vector_inventors");

        return rowDataset;

    }

    private Dataset<Row> transformInventorsToVector(Dataset<Row> dataset) {
        Word2Vec word2Vec = new Word2Vec();
        word2Vec.setInputCol("inventors_tokenized");
        word2Vec.setOutputCol("vector_inventors");
        word2Vec.setVectorSize(20);
        word2Vec.setMinCount(1);
        word2Vec.setNumPartitions(2);

        inventorsModel = word2Vec.fit(dataset);
        Dataset<Row> result = inventorsModel.transform(dataset);
        return result.drop("inventors_tokenized");
    }
}
