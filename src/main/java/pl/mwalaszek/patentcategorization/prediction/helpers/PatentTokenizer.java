package pl.mwalaszek.patentcategorization.prediction.helpers;

import org.apache.spark.ml.feature.StopWordsRemover;
import org.apache.spark.ml.feature.Tokenizer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import pl.mwalaszek.patentcategorization.domain.Patent;

public class PatentTokenizer {

    public Dataset<Row> tokenizeAndRemoveStopWords(Dataset<Patent> dataset){
        Tokenizer tokenizer = new Tokenizer()
                .setInputCol("pubAbstract")
                .setOutputCol("words");
        Dataset<Row> words = tokenizer.transform(dataset);
        StopWordsRemover stopWordsRemover = new StopWordsRemover()
                .setInputCol(tokenizer.getOutputCol())
                .setOutputCol("without_stop_words");
        return stopWordsRemover.transform(words);
    }

    public Dataset<Row> tokenizeInventors(Dataset<Row> dataset){
        Tokenizer tokenizer = new Tokenizer()
                .setInputCol("inventors")
                .setOutputCol("inventors_tokenized");
        return tokenizer.transform(dataset).drop("inventors");
    }
}
