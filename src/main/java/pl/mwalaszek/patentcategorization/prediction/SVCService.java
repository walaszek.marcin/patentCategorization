package pl.mwalaszek.patentcategorization.prediction;

import com.google.common.base.Stopwatch;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.classification.LinearSVC;
import org.apache.spark.ml.classification.OneVsRest;
import org.apache.spark.ml.classification.OneVsRestModel;
import org.apache.spark.ml.feature.StringIndexer;
import org.apache.spark.ml.feature.StringIndexerModel;
import org.apache.spark.mllib.evaluation.MulticlassMetrics;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mwalaszek.patentcategorization.domain.Patent;
import pl.mwalaszek.patentcategorization.prediction.helpers.VectorService;
import scala.Tuple2;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

@Service
public class SVCService implements Serializable, ClassificationService {
    private static final String INDEXED_LABELS_COLUMN = "indexedLabel";
    private OneVsRestModel oneVsRestModel;
    private double precision;
    private double accuracy;
    private double recall;
    private VectorService vectorService;
    private String[] labels;
    private Map<String, Double> labelPrecisionMap = new HashMap<>();

    @Autowired
    public SVCService(VectorService vectorService) {
        this.vectorService = vectorService;
    }

    @Override
    public void setup(Dataset<Row> dataset) {
        Stopwatch timer = Stopwatch.createStarted();
        StringIndexerModel labelIndexer = createLabelIndexer(dataset);

        Dataset<Row>[] splits = dataset.randomSplit(new double[]{0.8, 0.2});
        Dataset<Row> trainingData = splits[0];
        Dataset<Row> testData = splits[1];

        LinearSVC lsvc = new LinearSVC()
                .setLabelCol(INDEXED_LABELS_COLUMN)
                .setFeaturesCol("vector")
                .setRegParam(0.1);

        OneVsRest ovr = new OneVsRest()
                .setLabelCol(INDEXED_LABELS_COLUMN)
                .setFeaturesCol("vector")
                .setClassifier(lsvc);

        Pipeline pipeline = new Pipeline()
                .setStages(new PipelineStage[]{labelIndexer, ovr});

        PipelineModel model = pipeline.fit(trainingData);
        this.oneVsRestModel = (OneVsRestModel) (model.stages()[1]);
        Dataset<Row> predictions = model.transform(testData);

        validateModel(predictions);
        System.out.println("SVC training time: " + timer.stop());
    }

    @Override
    public double getPrecision() {
        DecimalFormat df = new DecimalFormat("#.####");
        return Double.valueOf(df.format(this.precision));
    }

    @Override
    public double getAccuracy() {
        DecimalFormat df = new DecimalFormat("#.####");
        return Double.valueOf(df.format(this.accuracy));
    }

    @Override
    public double getRecall() {
        DecimalFormat df = new DecimalFormat("#.####");
        return Double.valueOf(df.format(this.recall));
    }

    @Override
    public Map<String, Double> getLabelPrecisionMap() {
        return labelPrecisionMap;
    }

    @Override
    public String predictCategory(Patent input) {
        Dataset<Row> dataWithVector = vectorService.transformToVector(input);
        Double predict = oneVsRestModel.transform(dataWithVector).first().getDouble(2);

        return labels[predict.intValue()];
    }

    private StringIndexerModel createLabelIndexer(Dataset<Row> dataset) {
        StringIndexerModel labelIndexer = new StringIndexer()
                .setInputCol("firstLevelCategory")
                .setOutputCol(INDEXED_LABELS_COLUMN)
                .fit(dataset);
        this.labels = labelIndexer.labels();
        return labelIndexer;
    }

    private void validateModel(Dataset<Row> predictions) {
        JavaRDD<Tuple2<Object, Object>> predictionsToTuple =
                predictions.javaRDD().map(row -> new Tuple2<>(row.getDouble(3),row.getDouble(2)));

        MulticlassMetrics metrics = new MulticlassMetrics(predictionsToTuple.rdd());
        getPrecisionForAllClasses(metrics);
        this.precision = metrics.precision();
        this.accuracy = metrics.accuracy();
        this.recall = metrics.recall();
    }

    private void getPrecisionForAllClasses(MulticlassMetrics metrics) {
        for (Double i=0.0; i<labels.length; i+=1.0) {
            labelPrecisionMap.put(labels[i.intValue()], new BigDecimal(metrics.precision(i)).setScale(7, BigDecimal.ROUND_HALF_UP).doubleValue());
        }
    }
}
