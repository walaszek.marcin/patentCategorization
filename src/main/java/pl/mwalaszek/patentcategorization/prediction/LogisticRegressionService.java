package pl.mwalaszek.patentcategorization.prediction;

import com.google.common.base.Stopwatch;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.ml.linalg.DenseVector;
import org.apache.spark.mllib.classification.ClassificationModel;
import org.apache.spark.mllib.classification.LogisticRegressionWithLBFGS;
import org.apache.spark.mllib.evaluation.MulticlassMetrics;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mwalaszek.patentcategorization.domain.Patent;
import pl.mwalaszek.patentcategorization.domain.PreditionAndLabel;
import pl.mwalaszek.patentcategorization.prediction.helpers.VectorService;
import scala.Tuple2;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

@Service
public class LogisticRegressionService implements Serializable, ClassificationService {
    private VectorService vectorService;
    private ClassificationModel logisticRegressionModel;
    private double precision;
    private double accuracy;
    private double recall;
    private BiMap<String, Double> labelsMap = HashBiMap.create();
    private Map<String, Double> labelPrecisionMap = new HashMap<>();


    @Autowired
    public LogisticRegressionService(VectorService vectorService) {
        this.vectorService = vectorService;
        initializeLabelsMap();
    }

    @Override
    public void setup(Dataset<Row> dataset) {
        Stopwatch timer = Stopwatch.createStarted();
        JavaRDD<LabeledPoint>[] splits = labelVectors(dataset).randomSplit(new double[] {0.8, 0.2});
        JavaRDD<LabeledPoint> training = splits[0];
        JavaRDD<LabeledPoint> testing = splits[1];

        LogisticRegressionWithLBFGS regression = new LogisticRegressionWithLBFGS();
        regression.setNumClasses(labelsMap.size());
        regression.optimizer()
                .setNumIterations(100);
        this.logisticRegressionModel = regression.run(training.rdd());
        testModel(testing);
        System.out.println("Logistic Regression training time: " + timer.stop());
    }

    @Override
    public double getPrecision() {
        DecimalFormat df = new DecimalFormat("#.####");
        return Double.valueOf(df.format(this.precision));
    }

    @Override
    public double getAccuracy() {
        DecimalFormat df = new DecimalFormat("#.####");
        return Double.valueOf(df.format(this.accuracy));
    }

    @Override
    public double getRecall() {
        DecimalFormat df = new DecimalFormat("#.####");
        return Double.valueOf(df.format(this.recall));
    }

    @Override
    public Map<String, Double> getLabelPrecisionMap() {
        return labelPrecisionMap;
    }

    @Override
    public String predictCategory(Patent input) {
        Dataset<Row> dataWithVector = vectorService.transformToVector(input);
        double predictedLabel = logisticRegressionModel.predict(Vectors.fromML((DenseVector) dataWithVector.collectAsList().get(0).get(1)));
        return labelsMap.inverse().get(predictedLabel);
    }

    private void initializeLabelsMap() {
        labelsMap.put("B21", 0.);
        labelsMap.put("A01", 1.);
        labelsMap.put("D01", 2.);
        labelsMap.put("C02", 3.);
        labelsMap.put("A47", 4.);
        labelsMap.put("E21", 5.);
        labelsMap.put("E04", 6.);
        labelsMap.put("D06", 7.);
        labelsMap.put("G01", 8.);
        labelsMap.put("F02", 9.);
        labelsMap.put("F16", 10.);
        labelsMap.put("H02", 11.);
        labelsMap.put("C07", 12.);
        labelsMap.put("G06", 13.);
        labelsMap.put("H04", 14.);
        labelsMap.put("B60", 15.);
    }

    private void testModel(JavaRDD<LabeledPoint> testing){
        JavaRDD<PreditionAndLabel> preditionAndLabels = testing.map((Function<LabeledPoint, PreditionAndLabel>) value ->
                new PreditionAndLabel(logisticRegressionModel.predict(value.features()), value.label()));

        JavaRDD<Tuple2<Object, Object>> predictions =
                preditionAndLabels.map(preditionAndLabel ->
                        new Tuple2<>(preditionAndLabel.getPrediction(), preditionAndLabel.getLabel()));

        MulticlassMetrics metrics = new MulticlassMetrics(predictions.rdd());
        getPrecisionForAllClasses(metrics);
        this.precision = metrics.precision();
        this.accuracy = metrics.accuracy();
        this.recall = metrics.recall();
    }


    private JavaRDD<LabeledPoint> labelVectors(Dataset<Row> dataset) {
        return dataset.javaRDD().map(v1 -> new LabeledPoint(labelsMap.get(v1.get(0)), Vectors.fromML((DenseVector) v1.get(1))));
    }

    private void getPrecisionForAllClasses(MulticlassMetrics metrics) {
        for (Double i=0.0; i<labelsMap.size(); i+=1.0) {
            try {
                double precision = metrics.precision(i);
                labelPrecisionMap.put(labelsMap.inverse().get(i), new BigDecimal(precision).setScale(7, BigDecimal.ROUND_HALF_UP).doubleValue());
            } catch (Exception ignored){}
        }
    }
}
