package pl.mwalaszek.patentcategorization.prediction;

import com.google.common.base.Stopwatch;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.classification.RandomForestClassificationModel;
import org.apache.spark.ml.classification.RandomForestClassifier;
import org.apache.spark.ml.feature.StringIndexer;
import org.apache.spark.ml.feature.StringIndexerModel;
import org.apache.spark.ml.feature.VectorIndexer;
import org.apache.spark.ml.feature.VectorIndexerModel;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.mllib.evaluation.MulticlassMetrics;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mwalaszek.patentcategorization.domain.Patent;
import pl.mwalaszek.patentcategorization.prediction.helpers.VectorService;
import scala.Tuple2;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

@Service
public class RandomForrestService implements Serializable, ClassificationService {
    private static final String INDEXED_LABELS_COLUMN = "indexedLabel";
    private static final String INDEXED_FEATURES_COLUMN = "indexedFeatures";
    private double precision;
    private double accuracy;
    private double recall;
    private RandomForestClassificationModel randomForrestModel;
    private String[] labels;
    private VectorService vectorService;
    private Map<String, Double> labelPrecisionMap = new HashMap<>();

    @Autowired
    public RandomForrestService(VectorService vectorService) {
        this.vectorService = vectorService;
    }

    @Override
    public void setup(Dataset<Row> dataset) {
        Stopwatch timer = Stopwatch.createStarted();
        StringIndexerModel labelIndexer = createLabelIndexer(dataset);
        VectorIndexerModel featureIndexer = createFeatureIndexer(dataset);

        Dataset<Row>[] splits = dataset.randomSplit(new double[]{0.8, 0.2});
        Dataset<Row> trainingData = splits[0];
        Dataset<Row> testData = splits[1];

        RandomForestClassifier rf = new RandomForestClassifier()
                .setMaxDepth(20)
                .setLabelCol(INDEXED_LABELS_COLUMN)
                .setFeaturesCol(INDEXED_FEATURES_COLUMN);

        Pipeline pipeline = new Pipeline()
                .setStages(new PipelineStage[]{labelIndexer, featureIndexer, rf});

        PipelineModel pipelineModel = pipeline.fit(trainingData);
        validateModel(testData, pipelineModel);

        this.randomForrestModel = (RandomForestClassificationModel)(pipelineModel.stages()[2]);
        System.out.println("Random Forrest training time: " + timer.stop());
    }

    @Override
    public double getPrecision() {
        DecimalFormat df = new DecimalFormat("#.####");
        return Double.valueOf(df.format(this.precision));
    }

    @Override
    public double getAccuracy() {
        DecimalFormat df = new DecimalFormat("#.####");
        return Double.valueOf(df.format(this.accuracy));
    }

    @Override
    public double getRecall() {
        DecimalFormat df = new DecimalFormat("#.####");
        return Double.valueOf(df.format(this.recall));
    }

    public Map<String, Double> getLabelPrecisionMap() {
        return labelPrecisionMap;
    }

    @Override
    public String predictCategory(Patent input) {
        Dataset<Row> dataWithVector = vectorService.transformToVector(input);
        Double predict = randomForrestModel.predict((Vector) dataWithVector.collectAsList().get(0).get(1));

        return labels[predict.intValue()];
    }

    private void validateModel(Dataset<Row> testData, PipelineModel pipelineModel) {
        Dataset<Row> predictions = pipelineModel.transform(testData);
        JavaRDD<Tuple2<Object, Object>> predictionsAndLabels = predictions.javaRDD().map(row -> new Tuple2<>(row.getDouble(6), row.getDouble(2)));
        MulticlassMetrics metrics = new MulticlassMetrics(predictionsAndLabels.rdd());
        getPrecisionForAllClasses(metrics);

        this.precision = metrics.precision();
        this.accuracy = metrics.accuracy();
        this.recall = metrics.recall();
    }

    private void getPrecisionForAllClasses(MulticlassMetrics metrics) {
        for (Double i=0.0; i<labels.length; i+=1.0) {
            labelPrecisionMap.put(labels[i.intValue()], new BigDecimal(metrics.precision(i)).setScale(7, BigDecimal.ROUND_HALF_UP).doubleValue());
        }
    }

    private VectorIndexerModel createFeatureIndexer(Dataset<Row> rowDataset) {
        return new VectorIndexer()
                .setInputCol("vector")
                .setOutputCol(INDEXED_FEATURES_COLUMN)
                .setMaxCategories(4)
                .fit(rowDataset);
    }

    private StringIndexerModel createLabelIndexer(Dataset<Row> rowDataset) {
        StringIndexerModel labelIndexer = new StringIndexer()
                .setInputCol("firstLevelCategory")
                .setOutputCol(INDEXED_LABELS_COLUMN)
                .fit(rowDataset);
        this.labels = labelIndexer.labels();
        return labelIndexer;
    }
}
