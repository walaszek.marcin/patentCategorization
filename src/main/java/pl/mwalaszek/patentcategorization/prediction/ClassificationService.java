package pl.mwalaszek.patentcategorization.prediction;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import pl.mwalaszek.patentcategorization.domain.Patent;

import java.util.Map;

public interface ClassificationService {
    void setup(Dataset<Row> dataset);
    double getAccuracy();
    double getPrecision();
    double getRecall();
    Map<String, Double> getLabelPrecisionMap();
    String predictCategory(Patent input);
}
