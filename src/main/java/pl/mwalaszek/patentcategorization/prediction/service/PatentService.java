package pl.mwalaszek.patentcategorization.prediction.service;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mwalaszek.patentcategorization.domain.Patent;

import java.io.Serializable;

@Service
public class PatentService implements Serializable {
    private SparkSession sparkSession;

    @Autowired
    public PatentService(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public Dataset<Patent> getPatendDataset(){
        Encoder<Patent> patentEncoder = Encoders.bean(Patent.class);

        SQLContext sqlContext = new org.apache.spark.sql.SQLContext(sparkSession.sparkContext());

        return sqlContext
                .read()
                .option("multiLine", true)
                .json("data/fixed_patents.json")
                .map((MapFunction<Row, Patent>) value -> new Patent(value.getString(0), value.getString(2), value.getString(1)), patentEncoder);
    }
}
