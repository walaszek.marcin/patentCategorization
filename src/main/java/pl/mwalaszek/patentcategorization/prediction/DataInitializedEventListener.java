package pl.mwalaszek.patentcategorization.prediction;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pl.mwalaszek.patentcategorization.DataInitializedEvent;
import pl.mwalaszek.patentcategorization.prediction.helpers.VectorService;
import pl.mwalaszek.patentcategorization.prediction.service.PatentService;

@Component
public class DataInitializedEventListener {
    private final Logger LOG = Logger.getLogger(DataInitializedEventListener.class);
    private PatentService patentService;
    private LogisticRegressionService logisticRegressionService;
    private SVCService svcService;
    private RandomForrestService randomForrestService;
    private VectorService vectorService;

    @Autowired
    public DataInitializedEventListener(PatentService patentService, LogisticRegressionService logisticRegressionService,
                                        SVCService svcService, RandomForrestService randomForrestService, VectorService vectorService) {
        this.patentService = patentService;
        this.logisticRegressionService = logisticRegressionService;
        this.svcService = svcService;
        this.randomForrestService = randomForrestService;
        this.vectorService = vectorService;
    }

    @EventListener
    public void onApplicationEvent(DataInitializedEvent event) {
        LOG.info("Starting model build");

        Dataset<Row> vectors = vectorService.transformToVector(patentService.getPatendDataset());

        logisticRegressionService.setup(vectors);
        svcService.setup(vectors);
        randomForrestService.setup(vectors);

        LOG.info("Finished model build");
    }
}
