package pl.mwalaszek.patentcategorization.domain.google;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Publication {
    private String ipc_code;
    private String text;
    private String inventors;

    @JsonProperty("ipc_code")
    public String getIPC() { return ipc_code; }

    @JsonProperty("ipc_code")
    public void setIpc_code(String ipc_code) {
        this.ipc_code = ipc_code;
    }

    @JsonProperty("text")
    public String getText() { return text; }

    @JsonProperty("text")
    public void setText(String value) { this.text = value; }

    @JsonProperty("inventors")
    public String getInventors() {
        return inventors;
    }

    @JsonProperty("inventors")
    public void setInventors(String inventors) {
        this.inventors = inventors;
    }
}
