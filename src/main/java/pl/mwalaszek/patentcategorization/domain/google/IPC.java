package pl.mwalaszek.patentcategorization.domain.google;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IPC {
    private String code;
    private String inventive;
    private String first;

    @JsonProperty("code")
    public String getCode() { return code; }
    @JsonProperty("code")
    public void setCode(String value) { this.code = value; }

    @JsonProperty("inventive")
    public String getInventive() { return inventive; }
    @JsonProperty("inventive")
    public void setInventive(String value) { this.inventive = value; }

    @JsonProperty("first")
    public String getFirst() { return first; }
    @JsonProperty("first")
    public void setFirst(String value) { this.first = value; }
}
