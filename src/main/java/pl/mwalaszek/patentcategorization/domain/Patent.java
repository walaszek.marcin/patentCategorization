package pl.mwalaszek.patentcategorization.domain;

import jetbrains.exodus.entitystore.Entity;

public class Patent {
    private String firstLevelCategory;
    private String pubAbstract;
    private String inventors;

    public Patent() {
    }

    public Patent(String pubAbstract) {
        this.pubAbstract = pubAbstract;
        this.inventors = "";
    }

    public Patent(String firstLevelCategory, String pubAbstract, String inventors) {
        this.firstLevelCategory = firstLevelCategory;
        this.pubAbstract = pubAbstract;
        this.inventors = inventors;
    }

    public String getFirstLevelCategory() {
        return firstLevelCategory;
    }

    public void setFirstLevelCategory(String firstLevelCategory) {
        this.firstLevelCategory = firstLevelCategory;
    }

    public String getPubAbstract() {
        return pubAbstract;
    }

    public void setPubAbstract(String pubAbstract) {
        this.pubAbstract = pubAbstract;
    }

    public String getInventors() {
        return inventors;
    }

    public void setInventors(String inventors) {
        this.inventors = inventors;
    }

    public void toEntity(Entity entity) {
        entity.setProperty("firstLevelCategory", this.firstLevelCategory);
        entity.setProperty("pubAbstract", this.pubAbstract);
        entity.setProperty("inventors", this.inventors);
    }

    public static Patent fromEntity(Entity entity){
        return new Patent(
                (String) entity.getProperty("firstLevelCategory"),
                (String) entity.getProperty("pubAbstract"),
                (String) entity.getProperty("inventors"));
    }
}
