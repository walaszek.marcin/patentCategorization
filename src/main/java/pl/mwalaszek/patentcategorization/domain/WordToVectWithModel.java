package pl.mwalaszek.patentcategorization.domain;

import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.io.Serializable;

public class WordToVectWithModel implements Serializable {
    private Word2VecModel model;
    private Dataset<Row> result;

    public WordToVectWithModel(Word2VecModel model, Dataset<Row> result) {
        this.model = model;
        this.result = result;
    }

    public Word2VecModel getModel() {
        return model;
    }

    public Dataset<Row> getResult() {
        return result;
    }

    public void setResult(Dataset<Row> result) {
        this.result = result;
    }
}
