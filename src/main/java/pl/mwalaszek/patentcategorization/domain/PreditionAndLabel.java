package pl.mwalaszek.patentcategorization.domain;

import java.io.Serializable;

public class PreditionAndLabel implements Serializable {
    private Double prediction;
    private Double label;

    public PreditionAndLabel(Double prediction, Double label) {
        this.prediction = prediction;
        this.label = label;
    }

    public Double getPrediction() {
        return prediction;
    }

    public Double getLabel() {
        return label;
    }

    public boolean predictionOk(){
        return prediction.equals(label);
    }
}
