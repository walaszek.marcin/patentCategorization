package pl.mwalaszek.patentcategorization.data.repository;

import jetbrains.exodus.entitystore.Entity;
import jetbrains.exodus.entitystore.PersistentEntityStore;
import jetbrains.exodus.entitystore.PersistentEntityStores;
import pl.mwalaszek.patentcategorization.config.Constants;
import pl.mwalaszek.patentcategorization.domain.Patent;

import java.util.LinkedList;
import java.util.List;

@org.springframework.stereotype.Repository
public class XodusPatentRepository implements Repository<Patent> {

    private final PersistentEntityStore store = PersistentEntityStores.newInstance(Constants.patentStore);

    @Override
    public List<Patent> findAll() {
        return store.computeInReadonlyTransaction(txn -> {
            List<Patent> temp = new LinkedList<>();
            txn.getAll("Patent").forEach(entity -> temp.add(Patent.fromEntity(entity)));
            return temp;
        });
    }

    @Override
    public void saveAll(List<Patent> patentList) {
        patentList.forEach(patent -> store.executeInTransaction(txn -> {
            Entity entity = txn.newEntity("Patent");
            patent.toEntity(entity);
        }));
    }
}
