package pl.mwalaszek.patentcategorization.data.repository;

import java.util.List;

public interface Repository <T>{
    List<T> findAll();
    void saveAll(List<T> t);
}
