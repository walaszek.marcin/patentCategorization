package pl.mwalaszek.patentcategorization.data;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import pl.mwalaszek.patentcategorization.data.repository.XodusPatentRepository;
import pl.mwalaszek.patentcategorization.domain.Patent;
import pl.mwalaszek.patentcategorization.domain.google.Publication;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DataService {
    private static final Logger LOG = Logger.getLogger(DataService.class);
    private XodusPatentRepository patentRepository;

    public DataService(XodusPatentRepository patentRepository) {
        this.patentRepository = patentRepository;
    }

    public void intiDatabase() throws IOException {
        Gson gson = new Gson();
        List<Patent> patents = getPatentsFromFiles(gson);
        FileWriter writer = new FileWriter("data/fixed_patents.json");
        gson.toJson(patents, writer);
        writer.close();
        patentRepository.saveAll(patents);
        LOG.info("Overall number of patents: " + patents.size());
        LOG.info("Summary of categories: ");
        patents
                .stream()
                .collect(Collectors.groupingBy(Patent::getFirstLevelCategory))
                .forEach((s, patentList) -> LOG.info("Category: " + s + " size: " + patentList.size()));
    }

    private List<Patent> getPatentsFromFiles(Gson gson) throws IOException {
        List<Patent> patents = new ArrayList<>();
        for (int i = 1; i<60; i++) {
            String publicationsJson = new String(Files.readAllBytes(Paths.get("data/" + i +".json")));
            Publication[] publications = gson.fromJson(publicationsJson, Publication[].class);
            patents.addAll(mapToPatents(publications));
        }
        return patents;
    }

    private List<Patent> mapToPatents(Publication[] publications) {
        return Arrays.stream(publications)
                .filter(publication ->
                        publication.getIPC() != null &&
                        publication.getText() != null &&
                        publication.getInventors() != null)
                .map(publication -> (new Patent(getCategory(
                        publication.getIPC()),
                        publication.getText(), publication.getInventors())))
                .filter(patent -> !patent.getFirstLevelCategory().equals("0"))
                .collect(Collectors.toList());
    }

    private String getCategory(String ipc) {
        return ipc.substring(0, 3);
    }
}
